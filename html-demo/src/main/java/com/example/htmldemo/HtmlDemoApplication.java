package com.example.htmldemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class HtmlDemoApplication implements CommandLineRunner {

	@Autowired
	private JavaMailSender javaMailSender;

	public static void main(String[] args) {
		SpringApplication.run(HtmlDemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("Sending Email...");
        List<String> element = new ArrayList<>();
		try {
			HTMLTableVO htmlTableVO = getTableObject();
			WffTablePage htmlTable = new WffTablePage(htmlTableVO);
			htmlTable.afterPropertiesSet();
			System.out.println(htmlTable.toHtmlString());
            element.add(htmlTable.toHtmlString());
            element.add(htmlTable.toHtmlString());
            element.add(htmlTable.toHtmlString());
            WffPage wf = new WffPage(element);
			sendEmail(wf.toHtmlString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}

	void sendEmail(String emailBody) {
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage , true);
			messageHelper.setFrom("hi2rajnish@gmail.com");
			messageHelper.setTo("hi_rajnish@yahoo.com");
			messageHelper.setSubject("Email With Monthly Expenses");
			messageHelper.setText(emailBody, true);
			FileSystemResource file = new FileSystemResource("D:\\app\\myApps\\html-demo\\README.md");
			messageHelper.addAttachment(file.getFilename(),file);
		};
		javaMailSender.send(messagePreparator);
	}

	private  static HTMLTableVO getTableObject(){
		//String cssClassName = "demo";
		//String cssClassBody = ".demo {\n		border:1px solid #C0C0C0;\n		border-collapse:collapse;\n		padding:5px;\n	}\n	.demo th {\n		border:1px solid #C0C0C0;\n		padding:5px;\n		background:#F0F0F0;\n	}\n	.demo td {\n		border:1px solid #C0C0C0;\n		padding:5px;\n	}";

        String cssClassName = "tftable";
        String cssClassBody = ".tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n.tftable tr {background-color:#ffffff;}\n.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n.tftable tr:hover {background-color:#ffff99;}";

        String tableDescription = "Monthly Savings";
		String columnStyle = "\"color: rgb(0, 0, 0); font-family: \\\"Times New Roman\\\"; font-size: medium; text-align: left;\"";

		List<String> headerList = new ArrayList<>();
		headerList.add("Month");
		headerList.add("Saving");
		headerList.add("Expenses");

		List<List<String>> rowList = new ArrayList<>();
		List<String> rowItem1 = new ArrayList<>();
		rowItem1.add("Jan");
		rowItem1.add("$100");
		rowItem1.add("$90");
		List<String> rowItem2 = new ArrayList<>();
		rowItem2.add("Feb");
		rowItem2.add("$150");
		rowItem2.add("$100");
		rowList.add(rowItem1);
		rowList.add(rowItem2);

		return new HTMLTableVO(cssClassName,cssClassBody,tableDescription,columnStyle,headerList,rowList);
	}

}
