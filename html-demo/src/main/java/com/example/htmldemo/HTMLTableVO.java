package com.example.htmldemo;

import java.util.List;

public class HTMLTableVO {

    private String cssClassName;
    private String cssClassBody;
    private String tableDescription;
    private String columnStyle;
    private List<String> tableColumnNames;
    private List<List<String>> rowItems;

    public HTMLTableVO(String cssClassName, String cssClassBody, String tableDescription,
                       String columnStyle,List<String> tableColumnNames, List<List<String>> rowItems) {

        this.cssClassName = cssClassName;
        this.cssClassBody = cssClassBody;
        this.tableDescription = tableDescription;
        this.columnStyle = columnStyle;
        this.tableColumnNames = tableColumnNames;
        this.rowItems = rowItems;
    }

    public String getCssClassName() {
        return cssClassName;
    }

    public String getCssClassBody() {
        return cssClassBody;
    }

    public String getTableDescription() {
        return tableDescription;
    }

    public List<String> getTableColumnNames() {
        return tableColumnNames;
    }

    public List<List<String>> getRowItems() {
        return rowItems;
    }

    public String getColumnStyle() {
        return columnStyle;
    }
}
