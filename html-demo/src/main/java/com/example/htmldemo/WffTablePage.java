package com.example.htmldemo;

import com.webfirmframework.wffweb.tag.html.AbstractHtml;
import com.webfirmframework.wffweb.tag.html.Body;
import com.webfirmframework.wffweb.tag.html.Br;
import com.webfirmframework.wffweb.tag.html.Html;
import com.webfirmframework.wffweb.tag.html.attribute.global.ClassAttribute;
import com.webfirmframework.wffweb.tag.html.attribute.global.Style;
import com.webfirmframework.wffweb.tag.html.metainfo.Head;
import com.webfirmframework.wffweb.tag.html.stylesandsemantics.Span;
import com.webfirmframework.wffweb.tag.html.stylesandsemantics.StyleTag;
import com.webfirmframework.wffweb.tag.html.tables.*;
import com.webfirmframework.wffweb.tag.htmlwff.NoTag;

import java.util.List;

public class WffTablePage extends  Html{

    private HTMLTableVO htmlTableVO;

    public  WffTablePage(HTMLTableVO htmlTableVO){
        super(null);
        setPrependDocType(true);
        this.htmlTableVO = htmlTableVO;
    }

    private void generateHtml(){
        new Head(this).give(head -> {
            new StyleTag(head).give(style -> {
                new NoTag(style, htmlTableVO.getCssClassBody());
            });
            new NoTag(head, "\n");
        });

        new Body(this).give(body -> {
            new Table(body,
                    new ClassAttribute(htmlTableVO.getCssClassName())).give(table -> {
                new NoTag(table, "\n	");
                new Caption(table).give(caption -> {
                    new NoTag(caption, htmlTableVO.getTableDescription());
                });
                new NoTag(table, "\n	");
                setTableHeaders(table , htmlTableVO.getTableColumnNames(), htmlTableVO.getColumnStyle());
                new NoTag(table, "\n	");
                setRows(table , htmlTableVO.getRowItems());
                new NoTag(table, "\n");
            });
        });
    }

    private void setRows(AbstractHtml table , List<List<String>> rowList) {
        new TBody(table).give(tbody -> {
            rowList.stream().forEach(row -> {
                    new NoTag(tbody, "\n	");
                    new Tr(tbody).give(tr1 -> {
                        row.stream().forEach(cell -> {
                            new NoTag(tr1, "\n		");
                            new Td(tr1).give(td -> {
                                new NoTag(td, "&");
                                new NoTag(td, "nbsp;"+cell);
                            });
                            new NoTag(tr1, "\n		");
                        });
                    });
                    new NoTag(tbody, "\n	");
            });
        });
    }

    private void setTableHeaders(AbstractHtml table , List<String> headerList , String styles) {
        new THead(table).give(thead -> {
            new NoTag(thead, "\n	");
            new Tr(thead).give(tr -> {
                new NoTag(tr, "\n		");
                headerList.stream().forEach( header -> {
                    new Th(tr).give(th -> {
                        new Span(th,
                                new Style(styles)).give(span -> {
                            new NoTag(span, header);
                        });
                        new Br(th);
                    });
                    new NoTag(tr, "\n		");
                });
            });
            new NoTag(thead, "\n	");
        });
    }

    public void afterPropertiesSet(){
        generateHtml();
    }
}
