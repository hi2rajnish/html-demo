package com.example.htmldemo;

import com.webfirmframework.wffweb.tag.html.Body;
import com.webfirmframework.wffweb.tag.html.Html;
import com.webfirmframework.wffweb.tag.html.metainfo.Head;
import com.webfirmframework.wffweb.tag.html.stylesandsemantics.Div;
import com.webfirmframework.wffweb.tag.htmlwff.NoTag;

import java.util.ArrayList;
import java.util.List;

public class WffPage extends  Html{

    private List<String> element;
    public WffPage(List<String> element){
        super(null);
        this.element = element;
        setPrependDocType(true);
    }

    public String generateHtml(){
        new Body(this).give(body -> {
            element.stream().forEach( e -> {
                new Div(body).give(div -> {
                    new NoTag(div, " ");
                    new Div(div).give(div1 -> {
                        new NoTag(div1, e);
                    });
                    new NoTag(div, " ");
                });
            });
        });
        return this.toHtmlString();
    }

    public static void main(String[] args) {
        
        String str = "<!DOCTYPE html>" +
                "<html><head><style>.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}" +
                ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}" +
                ".tftable tr {background-color:#ffffff;}" +
                ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}" +
                ".tftable tr:hover {background-color:#ffff99;}</style>" +
                "</head><body><table class=\"tftable\">" +
                "<caption>Monthly Savings</caption>" +
                "<thead>" +
                "<tr>" +
                "<th><span style=\"\"color:rgb(0, 0, 0);font-family:\\\"Times New Roman\\\";font-size:medium;text-align:left;\">Month</span><br/></th>" +
                "<th><span style=\"\"color:rgb(0, 0, 0);font-family:\\\"Times New Roman\\\";font-size:medium;text-align:left;\">Saving</span><br/></th>" +
                "<th><span style=\"\"color:rgb(0, 0, 0);font-family:\\\"Times New Roman\\\";font-size:medium;text-align:left;\">Expenses</span><br/></th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td>&nbsp;Jan</td>" +
                "" +
                "<td>&nbsp;$100</td>" +
                "" +
                "<td>&nbsp;$90</td>" +
                "</tr>" +
                "" +
                "<tr>" +
                "<td>&nbsp;Feb</td>" +
                "" +
                "<td>&nbsp;$150</td>" +
                "" +
                "<td>&nbsp;$100</td>" +
                "</tr>" +
                "</tbody>" +
                "</table></body></html>";
        
        List<String> element = new ArrayList<>();
        element.add("One");
        element.add("Two");
        element.add(str);
        element.add(str);
        WffPage wf = new WffPage(element);
        System.out.println(wf.generateHtml());
    }
}
